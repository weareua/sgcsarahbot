import random
import time
import telegram
import re
from telegram import InputMediaPhoto
from lingua import greetings_list
from config import CHAT_ID
from log import logging


class Engine():
    """
    Creates engine object and proceed essential methods
    """
    inited = False

    @staticmethod
    def init(bot, update):
        """
        Init static members for entire class
        """
        Engine.bot = bot
        Engine.chat_id = CHAT_ID
        Engine.sender_name = update.message.from_user.first_name
        Engine.inited = True

        Engine.bot.setWebhook()

    def typing_time(self):
        """
        Simulate typing action
        """
        seconds_list = range(1,5)
        self.bot.sendChatAction(
            chat_id=self.chat_id, action=telegram.ChatAction.TYPING, timeout=200)
        time.sleep(random.choice(seconds_list))

    def greetings(self):
        """
        Generate and send greetings if someone appeals to bot
        """

        text_raw = random.choice(greetings_list)
        if self.yes_or_no():
            attr_list = [text_raw, self.sender_name]
            random.shuffle(attr_list)
            greetings_text = "{0}, {1}".format(attr_list[0], attr_list[1])
            if not greetings_text[0].isupper():
                greetings_text = greetings_text[:1].capitalize() + greetings_text[1:]
            self.send("", greetings_text)
        else:
            self.send("", text_raw.capitalize())

    def yes_or_no(self):
        quess_list = range(3)
        quess = random.choice(quess_list)
        if quess == 2:
            return True
        return False

    def strip_tags(self, message):
        tags = ['<p>', "<br />", '</p>', '<hr />', '<hr>']
        for tag in tags:
            if tag == "</p>":
                message = message.replace(tag, '\n')
            else:
                message = message.replace(tag, "")
        return message

    def strip_double_tags(self, message):
        double_tag_list = ['span id=', 'iframe', 'blockquote', 'script']
        closing_tag_prefix = "</"
        for tag in double_tag_list:
            text_list = message.split()
            if tag == 'span id=':
                closing_tag = '</span>'
            else:
                closing_tag = closing_tag_prefix + tag + ">"
            first_index = message.find("<" + tag)
            if first_index != -1:
                last_index = message.find(closing_tag, first_index) + len(closing_tag)
                part = message[first_index:last_index]
                message = message.replace(part,"")
        return message

    def strip_links(self, message):
        links = ['https://twitter.com', 'https://www.facebook.com']
        closing_tag = "</a>"
        for link in links:
            text_list = message.split()
            for text in text_list:
                first_index = message.find('<a href="{0}'.format(link))
                if first_index != -1:
                    last_index = message.find(closing_tag, first_index) + len(closing_tag)
                    part = message[first_index:last_index]
                    message = message.replace(part,"")
        return message

    def censor(self, message):
        abra = "#&!^#*^&%"
        parts = ['ёб', 'ебать', 'хуй', 'пизд', 'хуе', 'бля', 'хер']
        for part in parts:
            part_upper = part.upper()
            message_upper = message.upper()
            if part_upper in message_upper:
                start_index = -1
                while True:
                    # Advance start_index by 1.
                    start_index = message_upper.find(part_upper, start_index + 1)
                    # Break if not found.
                    if start_index == -1:
                        break
                    end_index = start_index + len(part)
                    message = message.replace(message[start_index:end_index], abra[:len(part)])
        return message

    def remove_HTML_tag(self, tag, string):
        string = re.sub(r"<\b(" + tag + r")\b[^>]*>", r"", string)
        return re.sub(r"<\/\b(" + tag + r")\b[^>]*>", r"", string)

    def get_images(self, message, img_list):
        img_list = re.findall(r'\ssrc="([^"]+)"', message)
        message = self.remove_HTML_tag("img", message)
        return message, img_list

    def send(self, title, message):
        """
        Send message
        """

        img_list = []
        message = self.strip_tags(message)
        message = self.strip_links(message)
        message = self.strip_double_tags(message)
        message, img_list = self.get_images(message, img_list)
        # censorship
        title = self.censor(title)
        message = self.censor(message)

        # append title to message
        if img_list or message:
            message = "{0}\n{1}".format(title, message.strip())

        try:
            if message.replace("\n", ""):
                logging.info("Sending text:{0}".format(message))
                self.typing_time()
                if (len(message) < 4000) and not "<a href=" in message:
                    self.bot.sendMessage(
                        chat_id=self.chat_id, text=message, parse_mode=telegram.ParseMode.HTML, timeout=200)
                else:
                    raise ValueError("Message is too large or contains restricted symbols")
            if len(img_list) > 1:
                photos_list = []
                for url in img_list:
                    photos_list.append(InputMediaPhoto(media=url))
                self.bot.sendMediaGroup(chat_id=self.chat_id, media=photos_list, timeout=200)
            elif len(img_list) == 1:
                # send gifs
                if img_list[0][-3:] == "gif":
                    self.bot.sendChatAction(chat_id=self.chat_id, action=telegram.ChatAction.UPLOAD_PHOTO)
                    self.bot.sendDocument(chat_id=self.chat_id, document=img_list[0])
                else:
                    self.bot.send_photo(chat_id=self.chat_id, photo=img_list[0], timeout=200)
        except Exception as e:
            logging.error("Error in message sending process:{0}".format(e))
