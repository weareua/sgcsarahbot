import threading
import atoma, requests
import re
import logging
from datetime import datetime, timedelta, timezone
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from config import REFRESH_TIME, TOKEN, RESTRICTED_CATEGORIES, POSTS_TO_FETCH, SOURCES
from engine import Engine
from log import logging


last_activity_time = ""


def set_last_activity_date(timestamp=False):
    global last_activity_time
    if isinstance(timestamp, datetime):
        last_activity_time = timestamp
    else:
        last_activity_time = datetime.now(timezone.utc) + timedelta(hours=3)
    return last_activity_time

def check_last_activity_date():
    if isinstance(last_activity_time, datetime):
        return True
    else:
        return False


def received_information(bot, update, user_data):
    text = update.message.text
    Engine.init(bot, update)
    engine = Engine()
    #Manners
    if re.match(r'(?i)(.*?)(п\и?р\о?\и?\е?\ю?в\w?|д\а?о?\ров\w?|прю|хай|алоха|оха\й?\о?\ё?|др\і?\и?с\ь?т\і?\и?)\s?', text):
        if not engine.yes_or_no():
            engine.greetings()
    # Posts
    if(update.message.chat_id and update.message.chat_id == engine.chat_id):
        # Start process only once
        if not check_last_activity_date():
            last_activity_time = set_last_activity_date()
            logging.info('Last parser activity at {}'.format(str(last_activity_time)))
            send_posts(engine, last_activity_time)
    

def send_posts(engine, last_activity_time):

    for source_title, url in SOURCES.items():

        response = requests.get(url)
        feed = atoma.parse_rss_bytes(response.content)

        items = feed.items[:POSTS_TO_FETCH]
        latest_post_timestamp = items[0].pub_date + timedelta(hours=3)

        # move on if at least latest post is new:
        logging.info('Latest post timestamp in {0}: {1}'.format(source_title, str(latest_post_timestamp)))
        if latest_post_timestamp > last_activity_time:
            logging.info("New post was fetched from " + source_title)
            for item in items:
                logging.info("Post categories: " + ", ".join(item.categories))
                if (item.pub_date  + timedelta(hours=3)) > last_activity_time:
                    logging.info("Posting this item: {0}".format(item.title))
                    if not any(elem in RESTRICTED_CATEGORIES  for elem in item.categories):
                        if item.content_encoded is None:
                            engine.send(item.title, item.description)
                        else:
                            engine.send(item.title, item.content_encoded)
            # Update last time activity
            latest_post_timestamp = set_last_activity_date(latest_post_timestamp)
        else:
            logging.info("No new posts")
    
    threading.Timer(REFRESH_TIME, send_posts, [engine, latest_post_timestamp]).start()

def start(bot, update):
    """
    Generate and send start message
    """

    start_text = "Привіт, {0}. ".format(update.message.from_user.first_name)
    bot.sendMessage(
        chat_id=update.message.chat_id,
        text=start_text)


def main():
    updater = Updater(TOKEN)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(MessageHandler(
        Filters.text, received_information, pass_user_data=True))

    updater.start_polling()
    updater.idle()
